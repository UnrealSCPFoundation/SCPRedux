
#include "MMJoinServerSlot.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

#include "Menu/MMMainMenu.h"

bool UMMJoinServerSlot::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success)
	{
		return false;
	}
	return true;
}

void UMMJoinServerSlot::UpdateTextFromInfo(class UMMMainMenu* InParent, uint32 InIndex, const FString& InInfo)
{
	Parent = InParent;
	Index = InIndex;
	//ServerInfo.ServerNameString = InInfo.ServerNameString;
	//ServerInfo.ServerIPAddress = InInfo.ServerIPAddress;
	//ServerInfo.ServerPing = InInfo.ServerPing;

	ServerJoinButton->OnClicked.AddDynamic(this, &UMMJoinServerSlot::JoinFromSlot);

	ServerName->SetText(FText::FromString(InInfo));
	//ServerIPAddress->SetText(FText::FromString(InInfo));
	//ServerPing->SetText(FText::FromString(FString::FromInt(ServerInfo.ServerPing)));
}

void UMMJoinServerSlot::JoinFromSlot()
{
	UE_LOG(LogTemp, Warning, TEXT("Joining Server From Slot %d"), Index);
	Parent->SelectIndex(Index);
	Parent->JoinSessionServer();
}

