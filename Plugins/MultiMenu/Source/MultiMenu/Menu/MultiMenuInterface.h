// Robert Chubb - Parabolic Labs

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"
#include "UObject/Interface.h"

#include "MultiMenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMultiMenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MULTIMENU_API IMultiMenuInterface
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void Host(FString ServerName) = 0;// = 0; means an empty implementation ... i.e pure virtual
	virtual void Join(const FString& Address) = 0;
	virtual void JoinSessionSlot(uint32 InIndex) = 0;
	virtual void RefreshServerList() = 0;
	virtual void LoadMainMenu() = 0;
};
