// Robert Chubb - Parabolic Labs

#include "MMGameMenu.h"

#include "Components/Button.h"

bool UMMGameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(BackToGameButton != nullptr)) return false;
	BackToGameButton->OnClicked.AddDynamic(this, &UMMGameMenu::BackToGame);

	if (!ensure(QuitToMenuButton != nullptr)) return false;
	QuitToMenuButton->OnClicked.AddDynamic(this, &UMMGameMenu::QuitToMenu);

	if (!ensure(QuitGameButton != nullptr)) return false;
	QuitGameButton->OnClicked.AddDynamic(this, &UMMGameMenu::QuitGame);

	return true;
}

void UMMGameMenu::BackToGame()
{
	TeardownMenu();
}

void UMMGameMenu::QuitToMenu()
{
	if (MenuInterface != nullptr)
	{
		TeardownMenu();
		MenuInterface->LoadMainMenu();
	}
}

void UMMGameMenu::QuitGame()
{
	QuitGameFromMenu();
}



