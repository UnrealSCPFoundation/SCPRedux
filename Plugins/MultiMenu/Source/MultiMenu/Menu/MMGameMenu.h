// Robert Chubb - Parabolic Labs

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Menu/MMWidgetBase.h"

#include "MMGameMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIMENU_API UMMGameMenu : public UMMWidgetBase
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* BackToGameButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* QuitToMenuButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* QuitGameButton;

	UFUNCTION()
		void BackToGame();

	UFUNCTION()
		void QuitToMenu();

	UFUNCTION()
		void QuitGame();
};
