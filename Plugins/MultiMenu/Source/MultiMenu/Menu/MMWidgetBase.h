// Robert Chubb - Parabolic Labs

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "MultiMenuInterface.h"

#include "MMWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIMENU_API UMMWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetupMenu(); // Setup game for playing
	void TeardownMenu(); // Teardown the MainMenu widget and set

	void QuitGameFromMenu();

	void SetMenuInterface(IMultiMenuInterface* InMenuInterface);

protected:
	IMultiMenuInterface* MenuInterface;
};
