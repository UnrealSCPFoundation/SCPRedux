// Robert Chubb - Parabolic Labs

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/NetDriver.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"

#include "Menu/MultiMenuInterface.h"

#include "MMInstanceBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIMENU_API UMMInstanceBase : public UGameInstance, public IMultiMenuInterface
{
	GENERATED_BODY()

public:

	UMMInstanceBase(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MultiMenuGameInstance)
		FString MainMenuMapLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MultiMenuGameInstance)
		FString GameMapLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MultiMenuGameInstance)
		FString MainMenuLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MultiMenuGameInstance)
		FString GameMenuLocation;

	virtual void Init();

	UFUNCTION(Exec, BlueprintCallable)
		void Host(FString ServerName) override;

	UFUNCTION(Exec, BlueprintCallable)
		void Join(const FString& Address) override;

	UFUNCTION()
		void JoinSessionSlot(uint32 InIndex) override;

	UFUNCTION(Exec, BlueprintCallable)
		void RefreshServerList() override;

	UFUNCTION(BlueprintCallable)
		void LoadMenuWidget();

	UFUNCTION(BlueprintCallable)
		void LoadInGameMenuWidget();

	void LoadMainMenu() override;

	void OnNetworkConnectionError(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);

private:
	TSubclassOf<class UUserWidget> MenuClass;
	TSubclassOf<class UUserWidget> InGameMenuClass;

	class UMMMainMenu* MainMenu;
	
	void OnCreateSessionComplete(FName SessionName, bool Success);
	void OnDestroySessionComplete(FName SessionName, bool Success); 
	void OnFindSessionsComplete(bool Success);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	void CreateSession();

	FString DesiredServerName;

	IOnlineSessionPtr Session;

	TSharedPtr< class FOnlineSessionSearch > SessionSearch;
};
