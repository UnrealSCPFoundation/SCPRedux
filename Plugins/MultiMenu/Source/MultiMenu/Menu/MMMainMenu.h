// Robert Chubb - Parabolic Labs

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Menu/MultiMenuInterface.h"
#include "Menu/MMWidgetBase.h"
#include "Menu/MMJoinServerSlot.h"

#include "MMMainMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIMENU_API UMMMainMenu : public UMMWidgetBase
{
	GENERATED_BODY()
public:
	UMMMainMenu(const FObjectInitializer & ObjectInitializer);

	UFUNCTION(BlueprintCallable)
		void GenerateServerList(TArray<FString> ServerInfos);

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
		class UButton* MultiplayerButton;

	void SelectIndex(uint32 InIndex);

	void JoinSessionServer();
private:
	UPROPERTY(meta = (BindWidget))
		class UEditableTextBox* ServerHostName;

	UPROPERTY(meta = (BindWidget))
		class UButton* HostButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* JoinButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* RefreshServerListButton;
	
	UPROPERTY(meta = (BindWidget))
		class UEditableTextBox* IPAddressBox;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* ServerList;

	//TArray<class UMMJoinServerSlot*> ServerSlotList;

	TSubclassOf<class UUserWidget> JoinServerSlotClass;
	FString JoinServerSlotLocation;

	UFUNCTION(BlueprintCallable)
		void HostServer();

	UFUNCTION(BlueprintCallable)
		void JoinServer();
	
	UFUNCTION(BlueprintCallable)
		void RefreshServerList();

	TOptional<uint32> SelectedIndex;

protected:
	virtual bool Initialize();

	/*
	UPROPERTY(meta = (BindWidget))
	class UButton* JoinGameButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* OptionButton;

	UPROPERTY(meta = (BindWidget))
		class UWidgetSwitcher* MenuWidgetSwitcher;

	UPROPERTY(meta = (BindWidget))
		class UWidget* JoinGameMenu;

	UPROPERTY(meta = (BindWidget))
		class UWidget* MainMenu;

	UPROPERTY(meta = (BindWidget))
		class UButton* BackMainMenuButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* QuitGameButton;
	*/

	/*
	UFUNCTION()
		void BackToMainMenuFromJoin();

	UFUNCTION()
		void QuitGame();
	*/
};
