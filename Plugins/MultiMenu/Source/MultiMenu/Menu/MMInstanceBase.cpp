// Robert Chubb - Parabolic Labs

#include "MMInstanceBase.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "OnlineSessionSettings.h"

#include "Menu/MMWidgetBase.h"
#include "Menu/MMMainMenu.h"
#include "Menu/MMJoinServerSlot.h"

const static FName SESSION_NAME = TEXT("SessionName");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

UMMInstanceBase::UMMInstanceBase(const FObjectInitializer & ObjectInitializer)
{
	//MainMenuMapLocation = TEXT("/MultiMenu/MainMenuLevel");
	//GameMapLocation = TEXT("/MultiMenu/GameLevel?listen");
	MainMenuMapLocation = TEXT("/Game/SCP/Maps/MenuLevel");
	GameMapLocation = TEXT("/Game/SCP/Maps/Main?listen");

	//MainMenuLocation = TEXT("/MultiMenu/Menu/MainMenu");
	//GameMenuLocation = TEXT("/MultiMenu/Menu/GameMenu");
	MainMenuLocation = TEXT("/Game/SCP/HUD/MainMenu");
	GameMenuLocation = TEXT("/Game/SCP/HUD/GameMenu");


	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(*MainMenuLocation);
	if (!ensure(MenuBPClass.Class != nullptr)) return;
	MenuClass = MenuBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(*GameMenuLocation);
	if (!ensure(InGameMenuBPClass.Class != nullptr)) return;
	InGameMenuClass = InGameMenuBPClass.Class;
}

void UMMInstanceBase::Init()
{
	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;
	Engine->OnNetworkFailure().AddUObject(this, &UMMInstanceBase::OnNetworkConnectionError);

	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (Subsystem != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found -> suitable online subsystem %s"), *Subsystem->GetSubsystemName().ToString());
		Session = Subsystem->GetSessionInterface();
		if (Session.IsValid())
		{
			Session->OnCreateSessionCompleteDelegates.AddUObject(this, &UMMInstanceBase::OnCreateSessionComplete);
			Session->OnDestroySessionCompleteDelegates.AddUObject(this, &UMMInstanceBase::OnDestroySessionComplete);
			Session->OnFindSessionsCompleteDelegates.AddUObject(this, &UMMInstanceBase::OnFindSessionsComplete); 
			Session->OnJoinSessionCompleteDelegates.AddUObject(this, &UMMInstanceBase::OnJoinSessionComplete);

		}
	} else {
		UE_LOG(LogTemp, Warning, TEXT("Did not find suitable online subsystem."));
	}
}

void UMMInstanceBase::OnCreateSessionComplete(FName SessionName, bool Success)
{
	if (!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not create session."));
		return;
	}

	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;
	Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));

	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;
	World->ServerTravel(GameMapLocation);
}

void UMMInstanceBase::OnDestroySessionComplete(FName SessionName, bool Success)
{
	if (Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Destroying old session."));
		CreateSession();
	}
}

void UMMInstanceBase::OnFindSessionsComplete(bool Success)
{
	if (Success && SessionSearch.IsValid() && MainMenu != nullptr)
	{
		TArray<FString> ServerInfos;
		for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
		{
			FString TempName = SearchResult.GetSessionIdStr();
			int32 TempPing = SearchResult.PingInMs;

			UE_LOG(LogTemp, Warning, TEXT("Found session string : %s"), *TempName);

			//ServerInfos.Add(FServerJoinSlotInfo(TempName, TEXT("127.0.0.1"), TempPing));
			ServerInfos.Add(TempName);
		}
		MainMenu->GenerateServerList(ServerInfos);
		UE_LOG(LogTemp, Warning, TEXT("Finished Find Session"));
	}
}

void UMMInstanceBase::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (!Session.IsValid()) return;

	FString Address;
	if (!Session->GetResolvedConnectString(SessionName, Address)) {
		UE_LOG(LogTemp, Warning, TEXT("Could not get address string."));
		return;
	} else {
		Join(Address);
	}

	/*
	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;

	Engine->AddOnScreenDebugMessage(0, 5, FColor::Green, FString::Printf(TEXT("Joining %s"), *Address));

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
	*/
}

void UMMInstanceBase::Host(FString ServerName)
{
	DesiredServerName = ServerName;
	if (Session.IsValid()) 
	{
		auto ExistingSession = Session->GetNamedSession(SESSION_NAME);
		if (ExistingSession != nullptr)
		{
			Session->DestroySession(SESSION_NAME);
		}
		else
		{
			CreateSession();
		}
	}
}

void UMMInstanceBase::CreateSession()
{
	if (Session.IsValid())
	{
		FOnlineSessionSettings SessionSettings;
		if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
		{
			SessionSettings.bIsLANMatch = true;
		}
		else
		{
			SessionSettings.bIsLANMatch = false;
		}
		SessionSettings.NumPublicConnections = 5;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = false;
		SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, DesiredServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

		Session->CreateSession(0, SESSION_NAME, SessionSettings);
	}
}

void UMMInstanceBase::Join(const FString& Address)
{
	
	
	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;
	Engine->AddOnScreenDebugMessage(0, 5, FColor::Green, FString::Printf(TEXT("Joining %s"), *Address));

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;
	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);

	//if (MainMenu != nullptr)
	//{
		//MainMenu->TeardownMenu();
	//}
}

void  UMMInstanceBase::JoinSessionSlot(uint32 InIndex)
{
	if (!Session.IsValid()) return;
	if (!SessionSearch.IsValid()) return;

	
	if (MainMenu != nullptr)
	{
		 MainMenu->TeardownMenu();
	}

	if (SessionSearch->SearchResults.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot join session because session doesnt exist."));
	} 
	else if (SessionSearch->SearchResults.Num() >= (int32)InIndex) 
	{
		Session->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[InIndex]);
	}
}

void UMMInstanceBase::RefreshServerList()
{
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch.IsValid())	
	{
		UE_LOG(LogTemp, Warning, TEXT("Found -> Session Interface."));
		//SessionSearch->bIsLanQuery = true;


		if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
		{
			SessionSearch->MaxSearchResults = 100;
			//SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
		}
		else
		{
			SessionSearch->MaxSearchResults = 100;
			SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
		}


		UE_LOG(LogTemp, Warning, TEXT("Starting to find online sessions."));
		Session->FindSessions(0, SessionSearch.ToSharedRef());

		UE_LOG(LogTemp, Warning, TEXT("Generating Server List."));
		//SessionSearch->SortSearchResults();
	}
}

void UMMInstanceBase::OnNetworkConnectionError(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	LoadMainMenu();
}

void UMMInstanceBase::LoadMainMenu()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	PlayerController->ClientTravel(MainMenuMapLocation, ETravelType::TRAVEL_Absolute);
}

void UMMInstanceBase::LoadMenuWidget()
{
	if (!ensure(MenuClass != nullptr)) return;
	MainMenu = CreateWidget<UMMMainMenu>(this, MenuClass);
	if (!ensure(MainMenu != nullptr)) return;
	MainMenu->SetupMenu();
	MainMenu->SetMenuInterface(this);
	UE_LOG(LogTemp, Warning, TEXT(" Init Start up : Found -> class %s"), *MenuClass->GetName());
}

void UMMInstanceBase::LoadInGameMenuWidget()
{
	if (!ensure(InGameMenuClass != nullptr)) return;
	UMMWidgetBase* InGameMenu = CreateWidget<UMMWidgetBase>(this, InGameMenuClass);
	if (!ensure(InGameMenu != nullptr)) return;
	InGameMenu->SetupMenu();
	InGameMenu->SetMenuInterface(this);

	UE_LOG(LogTemp, Warning, TEXT(" Game Start up : Found -> class %s"), *InGameMenuClass->GetName());
}
