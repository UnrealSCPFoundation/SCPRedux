// Robert Chubb - Parabolic Labs

#include "MMWidgetBase.h"

void UMMWidgetBase::SetMenuInterface(IMultiMenuInterface* InMenuInterface)
{
	MenuInterface = InMenuInterface;
}

void UMMWidgetBase::SetupMenu()
{
	this->AddToViewport();
	
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeUIOnly InputModeUI;
	InputModeUI.SetWidgetToFocus(this->TakeWidget());
	InputModeUI.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeUI);
	PlayerController->bShowMouseCursor = true;
	
}

void UMMWidgetBase::TeardownMenu()
{
	this->RemoveFromViewport();
	
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeGameOnly InputModeGame;
	PlayerController->SetInputMode(InputModeGame);
	PlayerController->bShowMouseCursor = false;
	
}

void UMMWidgetBase::QuitGameFromMenu()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	PlayerController->ConsoleCommand("quit");
}
