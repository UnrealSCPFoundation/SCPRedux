#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Menu/MultiMenuInterface.h"
#include "Menu/MMWidgetBase.h"

#include "MMJoinServerSlot.generated.h"


USTRUCT(BlueprintType)
struct FServerJoinSlotInfo
{
	GENERATED_BODY()

	FServerJoinSlotInfo() 
	{
		ServerNameString = TEXT("LocalHost");
		ServerIPAddress = TEXT("127.0.0.1");
		ServerPing = 5;
	}

	FServerJoinSlotInfo(FString Name, FString IP, int32 Ping)
	{
		ServerNameString = Name;
		ServerIPAddress = IP;
		ServerPing = Ping;
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString ServerNameString;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString ServerIPAddress;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ServerPing;
};

UCLASS()
class MULTIMENU_API UMMJoinServerSlot : public UMMWidgetBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite)
		FServerJoinSlotInfo ServerInfo;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ServerName;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ServerIPAddress;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ServerPing;


	UFUNCTION()
		void UpdateTextFromInfo(class UMMMainMenu* InParent, uint32 InIndex, const FString& InInfo);

	UFUNCTION(BlueprintCallable)
		void JoinFromSlot();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* ServerJoinButton;

	UPROPERTY()
		class UMMMainMenu* Parent;

	uint32 Index;

protected:
	virtual bool Initialize();

};
