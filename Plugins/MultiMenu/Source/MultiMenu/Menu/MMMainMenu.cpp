// Robert Chubb - Parabolic Labs

#include "MMMainMenu.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

#include "MMJoinServerSlot.h"

UMMMainMenu::UMMMainMenu(const FObjectInitializer & ObjectInitializer)
{
	JoinServerSlotLocation = TEXT("/Game/SCP/HUD/SubWidgets/JoinServerSlot");
	ConstructorHelpers::FClassFinder<UUserWidget> ServerSlot(*JoinServerSlotLocation);
	if (!ensure(ServerSlot.Class != nullptr)) return;
	JoinServerSlotClass = ServerSlot.Class;
}


bool UMMMainMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success)
	{
		return false;
	}
	
	if (!ensure(HostButton != nullptr)) return false;
	HostButton->OnClicked.AddDynamic(this, &UMMMainMenu::HostServer);

	if (!ensure(JoinButton != nullptr)) return false;
	JoinButton->OnClicked.AddDynamic(this, &UMMMainMenu::JoinServer);

	if (!ensure(RefreshServerListButton != nullptr)) return false;
	RefreshServerListButton->OnClicked.AddDynamic(this, &UMMMainMenu::RefreshServerList);
	
	/*
	if (!ensure(JoinButton != nullptr)) return false;
	JoinButton->OnClicked.AddDynamic(this, &UMMMainMenu::OpenJoinMenu);
	if (!ensure(BackMainMenuButton != nullptr)) return false;
	BackMainMenuButton->OnClicked.AddDynamic(this, &UMMMainMenu::BackToMainMenuFromJoin);
	if (!ensure(QuitGameButton != nullptr)) return false;
	QuitGameButton->OnClicked.AddDynamic(this, &UMMMainMenu::QuitGame);
	*/

	return true;
	
}

void UMMMainMenu::HostServer()
{
	if (MenuInterface != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hosting Server")); 
		FString ServerName = ServerHostName->Text.ToString();
		MenuInterface->Host(ServerName);
	};
}

void UMMMainMenu::JoinServer()
{
	if (MenuInterface != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Joining Server"));
		if (!ensure(IPAddressBox != nullptr)) return;
		if (IPAddressBox->GetText().ToString().IsEmpty()) return;
		FString IPAddress = IPAddressBox->GetText().ToString();
		MenuInterface->Join(IPAddress);
	};
}

void UMMMainMenu::JoinSessionServer()
{
	if (SelectedIndex.IsSet() && MenuInterface != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Joining Session Server at selected index %d."), SelectedIndex.GetValue());
		MenuInterface->JoinSessionSlot(SelectedIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected index not set."));
	}
}

void UMMMainMenu::GenerateServerList(TArray<FString> ServerInfos)
{
	if (MenuInterface != nullptr)
	{
		UWorld* World;
		World = this->GetWorld();
		if (!ensure(World != nullptr)) return;

		if (!ensure(ServerList != nullptr)) return;
		ServerList->ClearChildren();

		uint32 i = 0;
		for (const FString& ServerInfo : ServerInfos)
		{
			UMMJoinServerSlot* ServerSlot = CreateWidget<UMMJoinServerSlot>(World, JoinServerSlotClass);
			if (!ensure(ServerSlot != nullptr)) return;
			ServerSlot->UpdateTextFromInfo(this, i, ServerInfo);

			++i;
			ServerList->AddChild(ServerSlot);

			UE_LOG(LogTemp, Warning, TEXT("Adding Server to List"));
		}
	}
}

void UMMMainMenu::RefreshServerList()
{
	//if (!ensure(MenuWidgetSwitcher != nullptr)) return;
	//if (!ensure(JoinGameMenu != nullptr)) return;
	//MenuWidgetSwitcher->SetActiveWidget(JoinGameMenu);

	if (MenuInterface != nullptr)
	{
		ServerList->ClearChildren();

		UE_LOG(LogTemp, Warning, TEXT("Refreshing Server List"));
		MenuInterface->RefreshServerList();
	}
}

void UMMMainMenu::SelectIndex(uint32 InIndex)
{
	SelectedIndex = InIndex;
}

/*
void UMMMainMenu::BackToMainMenuFromJoin()
{
	if (!ensure(MenuWidgetSwitcher != nullptr)) return;
	if (!ensure(MainMenu != nullptr)) return;

	MenuWidgetSwitcher->SetActiveWidget(MainMenu);

	UE_LOG(LogTemp, Warning, TEXT("Open Main Menu from Join"));
}

void UMMMainMenu::QuitGame()
{
	QuitGameFromMenu();
}
*/