/* TODO 
comment ccp and h
comment enums/macros/structs
make uniques/generics lists global
inline the place hall functions?
remove ctor
would removing endcaps give a better result than preserving a corner?
what effect would letting endcaps attach to other endcaps have?
read data from ini file?
restart if there's any missing rooms?
*/

#include "MapGenerator.h"
#include "Macros.h"

UMapGenerator::UMapGenerator() { }

void UMapGenerator::Init()
{
	int32 zoneHeight = (MapHeight - 4) * (4.0f / 13.0f);

	EZStart = 1; // 1
	EZEnd = EZStart + zoneHeight; // 5
	HCZStart = EZEnd + 1; // 6

	LCZEnd = MapHeight - 1; // 16
	LCZStart = LCZEnd - zoneHeight; // 12
	HCZEnd = LCZStart - 1; // 11

	EZLeft = 0;
	EZRight = MapWidth;
	HCZLeft = MapWidth / 15;
	HCZRight = MapWidth - (MapWidth / 15);
	LCZLeft = MapWidth / 7;
	LCZRight = MapWidth - (MapWidth / 7);

	Map.SetNum(MapWidth);
	RotMap.SetNum(MapWidth);
	for (int32 x = 0; x < MapWidth; x++)
	{
		Map[x].SetNum(MapHeight);
		RotMap[x].SetNum(MapHeight);
		for (int32 y = 0; y < MapHeight; y++)
		{
			Map[x][y] = "NoRoom";
			RotMap[x][y] = FRotator(0, 0, 0);
		}
	}
}

TArray<FRoomSpawn> UMapGenerator::PackageMap()
{
	TArray<FRoomSpawn> list;

	for (int32 x = 0; x < MapWidth; x++)
	{
		for (int32 y = 0; y < MapHeight; y++)
		{
			FName room = Map[x][y];
			if (room != "NoRoom")
			{
				FRoomSpawn tile;

				float fx = (x - (MapWidth / 2)) * 2048;
				float fy = (y - (MapHeight - 1)) * 2048;

				tile.Room = ClassRelation[room];
				tile.Transform = FTransform(RotMap[x][y], FVector(fx, fy, 0.0f));
				tile.RoomName = room;

				list.Push(tile);
			}
		}
	}

	return list;
}

TArray<FRoomSpawn> UMapGenerator::GenerateMap()
{
	int32 timeout = 10;

	while (true)
	{
		Init();

		ParseDataTable();
		MutateChances();

		int32 numHtE = (Rand.RandRange(0, 1) + Rand.RandRange(0, 2) + 3) * (MapWidth / 15.0f);
		TArray<int32> HtEPositions = RandomIndices(HCZLeft, HCZRight - 1, numHtE);
		int32 numLtH = (Rand.RandRange(0, 1) + Rand.RandRange(0, 2) + 3) * (MapWidth / 15.0f);
		TArray<int32> LtHPositions = RandomIndices(LCZLeft, LCZRight - 1, numLtH);

		GenerateZone(EZLeft, EZRight - 1, EZStart - 1, EZEnd - 2, 4 * (MapWidth / 15.0f), {}, HtEPositions, true);
		GenerateZone(HCZLeft, HCZRight - 1, HCZStart, HCZEnd - 2, 3 * (MapWidth / 15.0f), HtEPositions, LtHPositions, false);
		GenerateZone(LCZLeft, LCZRight - 1, LCZStart, LCZEnd - 1, Rand.RandRange(2, 6) * (MapWidth / 15.0f), LtHPositions, { MapWidth / 2 }, false);

		PlaceSpecial(HtEPositions, LtHPositions);
		RemoveHCZTile(HtEPositions, LtHPositions);

		GenerateEndcaps(EZStart, EZEnd - 2, EZUniqueEndcaps.Num());
		GenerateEndcaps(HCZStart, HCZEnd - 2, HCZUniqueEndcaps.Num());
		GenerateEndcaps(LCZStart, LCZEnd - 1, LCZUniqueEndcaps.Num());

		DetermineRooms();
		DetermineRotations();

		if (!ReportUnplacedRooms())
		{
			TIMEOUT(timeout);
			Rand.Initialize(Rand.GetCurrentSeed() + 1);
			Reset();
			continue;
		}

		return PackageMap();
	}

	return {};
}

void UMapGenerator::Reset()
{
	TArray<TMap<FName, float>*> generics =
	{
		&LCZGenericEndcaps,
		&LCZGenericHallways,
		&LCZGenericCorners,
		&LCZGenericThreeways,
		&LCZGenericFourways,
		&HCZGenericEndcaps,
		&HCZGenericHallways,
		&HCZGenericCorners,
		&HCZGenericThreeways,
		&HCZGenericFourways,
		&EZGenericEndcaps,
		&EZGenericHallways,
		&EZGenericCorners,
		&EZGenericThreeways,
		&EZGenericFourways
	};

	TArray<TArray<FName>*> uniques =
	{
		&LCZUniqueEndcaps,
		&LCZUniqueHallways,
		&LCZUniqueCorners,
		&LCZUniqueThreeways,
		&LCZUniqueFourways,
		&HCZUniqueEndcaps,
		&HCZUniqueHallways,
		&HCZUniqueCorners,
		&HCZUniqueThreeways,
		&HCZUniqueFourways,
		&EZUniqueEndcaps,
		&EZUniqueHallways,
		&EZUniqueCorners,
		&EZUniqueThreeways,
		&EZUniqueFourways
	};

	for (auto map : generics)
		map->Empty();

	for (auto list : uniques)
		list->Empty();
}

void UMapGenerator::RemoveHCZTile(TArray<int32> TopConstraint, TArray<int32> BottomConstraint)
{
	int32 topMin, topMax, bottomMin, bottomMax;
	EXTREMA(topMin, topMax, TopConstraint);
	EXTREMA(bottomMin, bottomMax, BottomConstraint);

	int32 timeout = 100;
	while (true)
	{
		int32 x, y;

		if (Rand.RandRange(0, 1))
			y = HCZStart;
		else
			y = HCZEnd - 2;

		if (y == HCZStart)
			x = Rand.RandRange(topMin, topMax);
		else
			x = Rand.RandRange(bottomMin, bottomMax);

		if (GetShape(x, y) == ERoomShapes::Hallway)
		{
			Map[x][y] = "NoRoom";
			break;
		}

		TIMEOUT(timeout);
	}
}

void UMapGenerator::GenerateEndcaps(int32 Start, int32 End, int32 DesiredNumber)
{
	int32 existing = 0;

	for (int32 x = 0; x < MapWidth; x++)
		for (int32 y = Start; y < End + 1; y++)
			if (Map[x][y] != "NoRoom" && GetShape(x, y) == ERoomShapes::Endcap)
				existing++;

	int32 timeout = 100 * (DesiredNumber - existing);
	bool locked = false;
	FIntPoint safeLoc(-1, -1);

	for (int32 i = 0; i < DesiredNumber - existing; )
	{
		int32 x = Rand.RandRange(0, MapWidth - 1);
		int32 y = Rand.RandRange(Start, End);

		TArray<bool> arr = GetSurroundings(x, y);
		FIntPoint parentLoc;
		if (arr[0])
			parentLoc = { x, y - 1 };
		else if (arr[1])
			parentLoc = { x, y + 1 };
		else if (arr[2])
			parentLoc = { x - 1, y };
		else if (arr[3])
			parentLoc = { x + 1, y };
		else
			continue;

		ERoomShapes parent = GetShape(parentLoc.X, parentLoc.Y);

		if (parent == ERoomShapes::Corner && !locked)
		{
			safeLoc = { parentLoc.X, parentLoc.Y };
			locked = true;
		}

		if (parentLoc.X == safeLoc.X && parentLoc.Y == safeLoc.Y)
			continue;

		if (Map[x][y] == "NoRoom" && GetShape(x, y) == ERoomShapes::Endcap && parent != ERoomShapes::Endcap)
		{
			Map[x][y] = "SomeRoom";
			i++;
		}

		TIMEOUT(timeout);
	}
}

void UMapGenerator::GenerateZone(int32 MinX, int32 MaxX, int32 MinY, int32 MaxY, int32 NumHalls, TArray<int32> TopConstraint, TArray<int32> BottomConstraint, bool MoreCorners)
{
	TArray<int32> halls = RandomIndices(MinX, MaxX, NumHalls);

	if (halls.Num() == 0)
		return;

	for (auto& index : halls)
		PlaceShortHall(MinY + 1, MaxY - 1, index);

	int32 minIndex, maxIndex;
	EXTREMA(minIndex, maxIndex, halls);

	int32 minIndexTop, maxIndexTop, minIndexBottom, maxIndexBottom;

	TopConstraint.Append({ minIndex, maxIndex });
	BottomConstraint.Append({ minIndex, maxIndex });

	EXTREMA(minIndexTop, maxIndexTop, TopConstraint);
	EXTREMA(minIndexBottom, maxIndexBottom, BottomConstraint);

	if (MoreCorners)
	{
		PlaceLongHall(minIndexTop, maxIndexTop, MinY);
		PlaceLongHall(minIndexBottom, maxIndexBottom, MaxY);
	}
	else
	{
		PlaceLongHall(Rand.RandRange(MinX, minIndexTop), Rand.RandRange(maxIndexTop, MaxX), MinY);
		PlaceLongHall(Rand.RandRange(MinX, minIndexBottom), Rand.RandRange(maxIndexBottom, MaxX), MaxY);
	}

	int32 height = (MaxY - 1) - (MinY + 1) + 1;
	int32 extra = (height - 2) / 3;
	for (int32 i = 0; i < extra; i++)
	{
		int32 index = ((i + 1.0f) / (extra + 1.0f)) * height;
		index += MinY + 1;
		PlaceLongHall(Rand.RandRange(MinX, minIndex), Rand.RandRange(maxIndex, MaxX), index);
	}
}

void UMapGenerator::PlaceSpecial(TArray<int32> HtEPositions, TArray<int32> LtHPositions)
{
	float gothruchance = 0.6f;

	// Place Gates

	TArray<int32> temp;
	for (int32 i = 0; i < MapWidth; i++)
	{
		if (Map[i][1] != "NoRoom")
			temp.Push(i);
		Map[i][0] = "NoRoom";
	}

	int32 index;
	if (temp.Num() > 0)
	{
		index = Rand.RandRange(0, temp.Num() - 1);
		Map[temp[index]][0] = "gateaentrance";
		temp.RemoveAt(index);
	}

	if (temp.Num() > 0)
	{
		index = Rand.RandRange(0, temp.Num() - 1);
		Map[temp[index]][0] = "exit1";
	}

	// Place Heavy to Entrance transitions and connected hallways

	int32 min, max;
	EXTREMA(min, max, HtEPositions);
	for (auto& num : HtEPositions)
	{
		Map[num][EZEnd] = "checkpoint2";
		if (Rand.FRand() < gothruchance || num == min || num == max)
			Map[num][EZEnd - 1] = "SomeRoom";
	}

	// Place Light to Heavy transitions and connected hallways

	EXTREMA(min, max, LtHPositions);
	for (auto& num : LtHPositions)
	{
		Map[num][HCZEnd] = "checkpoint1";
		if (Rand.FRand() < gothruchance || num == min || num == max)
			Map[num][HCZEnd - 1] = "SomeRoom";
	}

	// Place 173's room

	Map[MapWidth / 2][LCZEnd] = "start";
}

void UMapGenerator::PlaceLongHall(int32 MinX, int32 MaxX, int32 YPosition)
{
	for (int32 x = MinX; x < MaxX + 1; x++)
		Map[x][YPosition] = "SomeRoom";
}

void UMapGenerator::PlaceShortHall(int32 MinY, int32 MaxY, int32 XPosition)
{
	for (int32 y = MinY; y < MaxY + 1; y++)
		Map[XPosition][y] = "SomeRoom";
}

TArray<bool> UMapGenerator::GetSurroundings(int32 X, int32 Y)
{
	bool up, down, left, right;
	up = down = left = right = false;

	if (Y > 0)
	{
		if (Map[X][Y - 1] != "NoRoom")
			up = true;
	}

	if (Y < MapHeight - 1)
	{
		if (Map[X][Y + 1] != "NoRoom")
			down = true;
	}

	if (X > 0)
	{
		if (Map[X - 1][Y] != "NoRoom")
			left = true;
	}

	if (X < MapWidth - 1)
	{
		if (Map[X + 1][Y] != "NoRoom")
			right = true;
	}

	return { up, down, left, right };
}

ERoomShapes UMapGenerator::GetShape(int32 X, int32 Y)
{
	TArray<bool> arr = GetSurroundings(X, Y);
	bool up = arr[0];
	bool down = arr[1];
	bool left = arr[2];
	bool right = arr[3];

	switch (up + down + left + right)
	{
	case 1:
		return ERoomShapes::Endcap;
	case 2:
		return ((up && down) || (left && right)) ? ERoomShapes::Hallway : ERoomShapes::Corner;
	case 3:
		return ERoomShapes::Threeway;
	case 4:
		return ERoomShapes::Fourway;
	default:
		return ERoomShapes::Special;
	}
}

ERoomZones UMapGenerator::GetZone(int32 X, int32 Y)
{
	if (Y >= EZStart && Y < EZEnd)
		return ERoomZones::Entrance;

	if (Y >= HCZStart && Y < HCZEnd)
		return ERoomZones::Heavy;

	if (Y >= LCZStart && Y < LCZEnd)
		return ERoomZones::Light;

	return ERoomZones::Special;
}

void UMapGenerator::DetermineRooms()
{
	TArray<FIntPoint> coords;
	for (int32 x = 0; x < MapWidth; x++)
		for (int32 y = 0; y < MapHeight; y++)
			coords.Add({ x, y });

	while (coords.Num() > 0)
	{
		int32 index = Rand.RandRange(0, coords.Num() - 1);
		FIntPoint point = coords[index];
		coords.RemoveAt(index);

		ERoomZones zone = GetZone(point.X, point.Y);
		if (Map[point.X][point.Y] == "SomeRoom")
		{
			ERoomShapes shape = GetShape(point.X, point.Y);
			Map[point.X][point.Y] = ChooseRoom(zone, shape);
		}
	}
}

FName UMapGenerator::GetNextRoom(TArray<FName>& Uniques, TMap<FName, float>& Generics)
{
	if (Uniques.Num() > 0)
		return Uniques.Pop();

	float random = Rand.FRand();
	for (auto& pair : Generics)
		if (pair.Value > random)
			return pair.Key;

	return "endroom";
}

FName UMapGenerator::ChooseRoom(ERoomZones Zone, ERoomShapes Shape)
{
	switch (Zone)
	{
	case ERoomZones::Light:
		switch (Shape)
		{
		case ERoomShapes::Endcap:
			return GetNextRoom(LCZUniqueEndcaps, LCZGenericEndcaps);
		case ERoomShapes::Hallway:
			return GetNextRoom(LCZUniqueHallways, LCZGenericHallways);
		case ERoomShapes::Corner:
			return GetNextRoom(LCZUniqueCorners, LCZGenericCorners);
		case ERoomShapes::Threeway:
			return GetNextRoom(LCZUniqueThreeways, LCZGenericThreeways);
		case ERoomShapes::Fourway:
			return GetNextRoom(LCZUniqueFourways, LCZGenericFourways);
		}
		break;

	case ERoomZones::Heavy:
		switch (Shape)
		{
		case ERoomShapes::Endcap:
			return GetNextRoom(HCZUniqueEndcaps, HCZGenericEndcaps);
		case ERoomShapes::Hallway:
			return GetNextRoom(HCZUniqueHallways, HCZGenericHallways);
		case ERoomShapes::Corner:
			return GetNextRoom(HCZUniqueCorners, HCZGenericCorners);
		case ERoomShapes::Threeway:
			return GetNextRoom(HCZUniqueThreeways, HCZGenericThreeways);
		case ERoomShapes::Fourway:
			return GetNextRoom(HCZUniqueFourways, HCZGenericFourways);
		}
		break;

	case ERoomZones::Entrance:
		switch (Shape)
		{
		case ERoomShapes::Endcap:
			return GetNextRoom(EZUniqueEndcaps, EZGenericEndcaps);
		case ERoomShapes::Hallway:
			return GetNextRoom(EZUniqueHallways, EZGenericHallways);
		case ERoomShapes::Corner:
			return GetNextRoom(EZUniqueCorners, EZGenericCorners);
		case ERoomShapes::Threeway:
			return GetNextRoom(EZUniqueThreeways, EZGenericThreeways);
		case ERoomShapes::Fourway:
			return GetNextRoom(EZUniqueFourways, EZGenericFourways);
		}
		break;
	}
	return "NoRoom";
}

void UMapGenerator::DetermineRotations()
{
	FRotator rotUp(0, 0, 0);
	FRotator rotDown(0, 180, 0);
	FRotator rotLeft(0, -90, 0);
	FRotator rotRight(0, 90, 0);

	for (int32 x = 0; x < MapWidth; x++)
	{
		for (int32 y = 0; y < MapHeight; y++)
		{
			if (Map[x][y] == "NoRoom")
				continue;

			TArray<bool> arr = GetSurroundings(x, y);
			bool up = arr[0];
			bool down = arr[1];
			bool left = arr[2];
			bool right = arr[3];

			FRotator rotation;
			switch (GetShape(x, y))
			{
			case ERoomShapes::Endcap:
				if (up)
					rotation = rotUp;
				else if (down)
					rotation = rotDown;
				else if (left)
					rotation = rotLeft;
				else if (right)
					rotation = rotRight;
				break;

			case ERoomShapes::Hallway:
				if (up)
					rotation = (Rand.RandRange(0, 1)) ? rotUp : rotDown;
				else
					rotation = (Rand.RandRange(0, 1)) ? rotLeft : rotRight;
				break;

			case ERoomShapes::Corner:
				if (up)
				{
					if (left)
						rotation = rotUp;
					else
						rotation = rotRight;
				}
				else
				{
					if (left)
						rotation = rotLeft;
					else
						rotation = rotDown;
				}
				break;

			case ERoomShapes::Threeway:
				if (!up)
					rotation = rotDown;
				else if (!down)
					rotation = rotUp;
				else if (!left)
					rotation = rotRight;
				else if (!right)
					rotation = rotLeft;
				break;

			case ERoomShapes::Fourway:
				rotation = FRotator(0, Rand.RandRange(0, 3) * 90, 0);
				break;
			}

			if (Map[x][y] == "checkpoint1" || Map[x][y] == "checkpoint2")
				rotation = rotUp;

			RotMap[x][y] = rotation;
		}
	}
}

TArray<int32> UMapGenerator::RandomIndices(int32 Min, int32 Max, int32 Number)
{
	TArray<int32> list;

	int32 timeout = 100 * Number;
	while (list.Num() < Number)
	{
		int32 value = Rand.RandRange(Min, Max);

		if (!list.Contains<int32>(value + 1) && !list.Contains<int32>(value - 1))
			list.AddUnique(value);

		TIMEOUT(timeout);
	}

	return list;
}

void UMapGenerator::MutateChances()
{
	TArray<TMap<FName, float>*> generics = 
	{
		&LCZGenericEndcaps,
		&LCZGenericHallways,
		&LCZGenericCorners,
		&LCZGenericThreeways,
		&LCZGenericFourways,
		&HCZGenericEndcaps,
		&HCZGenericHallways,
		&HCZGenericCorners,
		&HCZGenericThreeways,
		&HCZGenericFourways,
		&EZGenericEndcaps,
		&EZGenericHallways,
		&EZGenericCorners,
		&EZGenericThreeways,
		&EZGenericFourways
	};

	for (auto map : generics)
	{
		float total = 0.0f;
		for (auto& pair : *map)
			total += pair.Value;

		float previous = 0.0f;
		for (auto& pair : *map)
		{
			pair.Value /= total;
			pair.Value += previous;
			previous = pair.Value;
		}
	}
}

void UMapGenerator::ParseDataTable()
{
	for (auto& it : RoomsTable->RowMap)
	{
		FRoomAttributes* rowData = (FRoomAttributes*)(it.Value);
		FName type = it.Key;

		ClassRelation.Add(type, rowData->Room);

		switch (rowData->Zone)
		{
		case ERoomZones::Light:
			if (rowData->Chance > 0.0f)
			{
				switch (rowData->Shape)
				{
				case ERoomShapes::Endcap:
					LCZGenericEndcaps.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Hallway:
					LCZGenericHallways.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Corner:
					LCZGenericCorners.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Threeway:
					LCZGenericThreeways.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Fourway:
					LCZGenericFourways.Add(type, rowData->Chance);
					break;
				}
			}
			else
			{
				switch (rowData->Shape)
				{
				case ERoomShapes::Endcap:
					LCZUniqueEndcaps.Add(type);
					break;
				case ERoomShapes::Hallway:
					LCZUniqueHallways.Add(type);
					break;
				case ERoomShapes::Corner:
					LCZUniqueCorners.Add(type);
					break;
				case ERoomShapes::Threeway:
					LCZUniqueThreeways.Add(type);
					break;
				case ERoomShapes::Fourway:
					LCZUniqueFourways.Add(type);
					break;
				}
			}
			break;

		case ERoomZones::Heavy:
			if (rowData->Chance > 0.0f)
			{
				switch (rowData->Shape)
				{
				case ERoomShapes::Endcap:
					HCZGenericEndcaps.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Hallway:
					HCZGenericHallways.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Corner:
					HCZGenericCorners.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Threeway:
					HCZGenericThreeways.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Fourway:
					HCZGenericFourways.Add(type, rowData->Chance);
					break;
				}
			}
			else
			{
				switch (rowData->Shape)
				{
				case ERoomShapes::Endcap:
					HCZUniqueEndcaps.Add(type);
					break;
				case ERoomShapes::Hallway:
					HCZUniqueHallways.Add(type);
					break;
				case ERoomShapes::Corner:
					HCZUniqueCorners.Add(type);
					break;
				case ERoomShapes::Threeway:
					HCZUniqueThreeways.Add(type);
					break;
				case ERoomShapes::Fourway:
					HCZUniqueFourways.Add(type);
					break;
				}
			}
			break;

		case ERoomZones::Entrance:
			if (rowData->Chance > 0.0f)
			{
				switch (rowData->Shape)
				{
				case ERoomShapes::Endcap:
					EZGenericEndcaps.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Hallway:
					EZGenericHallways.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Corner:
					EZGenericCorners.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Threeway:
					EZGenericThreeways.Add(type, rowData->Chance);
					break;
				case ERoomShapes::Fourway:
					EZGenericFourways.Add(type, rowData->Chance);
					break;
				}
			}
			else
			{
				switch (rowData->Shape)
				{
				case ERoomShapes::Endcap:
					EZUniqueEndcaps.Add(type);
					break;
				case ERoomShapes::Hallway:
					EZUniqueHallways.Add(type);
					break;
				case ERoomShapes::Corner:
					EZUniqueCorners.Add(type);
					break;
				case ERoomShapes::Threeway:
					EZUniqueThreeways.Add(type);
					break;
				case ERoomShapes::Fourway:
					EZUniqueFourways.Add(type);
					break;
				}
			}
			break;
		}
	}
}

bool UMapGenerator::ReportUnplacedRooms()
{
	TArray<TArray<FName>> uniques = 
	{
		LCZUniqueEndcaps,
		LCZUniqueHallways,
		LCZUniqueCorners,
		LCZUniqueThreeways,
		LCZUniqueFourways,
		HCZUniqueEndcaps,
		HCZUniqueHallways,
		HCZUniqueCorners,
		HCZUniqueThreeways,
		HCZUniqueFourways,
		EZUniqueEndcaps,
		EZUniqueHallways,
		EZUniqueCorners,
		EZUniqueThreeways,
		EZUniqueFourways
	};

	TArray<FName> fatalRooms =
	{
		"room008", // unlock EZ
		"room2sl", // unlock HCZ
		"room2ccont", // unlock exits
		"room079", // unlock exits
		"room914", // get level 3 keycard
		"room2testroom2", // get level 2 keycard
		"room2closets", // get level 1 keycard
		"room2nuke", // allows all endings
		"room106" // get level 5 keycard
	};

	FString missingRooms;
	for (auto& list : uniques)
	{
		for (auto& room : list)
		{
			if (fatalRooms.Contains<FName>(room))
			{
				UE_LOG(LogTemp, Error, TEXT("Fatal generation error, %s missing. Restarting."), *room.ToString());
				return false;
			}

			missingRooms += room.ToString() + "\t";
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Seed: %i\tUnplaced rooms: %s"), Rand.GetCurrentSeed(), *missingRooms);

	return true;
}
