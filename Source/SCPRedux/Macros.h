

#pragma once

#include "CoreMinimal.h"
//#include "Macros.generated.h"


#define TIMEOUT(Counter) if(--Counter <= 0) { UE_LOG(LogTemp, Warning, TEXT("Timeout warning: %s"), *FString(__FUNCTION__)); break; }

#define EXTREMA(Min, Max, Array)           \
Min = Max = Array.Num() ? Array[0] : 0;    \
for (auto& value : Array) {                \
	if (value < Min) { Min = value; }      \
	else if (value > Max) { Max = value; } \
}
