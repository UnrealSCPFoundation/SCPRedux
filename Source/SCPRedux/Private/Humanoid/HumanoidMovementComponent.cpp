// SCP

#include "HumanoidMovementComponent.h"
#include "GameFramework/Character.h"

UHumanoidMovementComponent::UHumanoidMovementComponent(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{}

void UHumanoidMovementComponent::SetSprinting(bool bSprinting)
{
	bWantsToSprint = bSprinting;
}

float UHumanoidMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	if (bWantsToSprint)
	{
		MaxSpeed *= SprintSpeedMultiplier;
	}

	return MaxSpeed;
}

float UHumanoidMovementComponent::GetMaxAcceleration() const
{
	float MaxAccel = Super::GetMaxAcceleration();

	if (bWantsToSprint)
	{
		MaxAccel *= SprintAccelerationMultiplier;
	}

	return MaxAccel;
}

//Set input flags on character from saved inputs
void UHumanoidMovementComponent::UpdateFromCompressedFlags(uint8 Flags)//Client only
{
	Super::UpdateFromCompressedFlags(Flags);
	
	//The Flags parameter contains the compressed input flags that are stored in the saved move.
	//UpdateFromCompressed flags simply copies the flags from the saved move into the movement component.
	//It basically just resets the movement component to the state when the move was made so it can simulate from there.
	bWantsToSprint = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
}

//============================================================================================
//FNetworkPredictionData_Client_Humanoid
//============================================================================================
void FSavedMove_Humanoid::Clear()
{
	Super::Clear();
	//Clear variables back to their default values.
	bSavedWantsToSprint = false;
}

uint8 FSavedMove_Humanoid::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();

	if (bSavedWantsToSprint)
	{
		Result |= FLAG_Custom_0;
	}

	return Result;
}

bool FSavedMove_Humanoid::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	//This pretty much just tells the engine if it can optimize by combining saved moves. There doesn't appear to be
	//any problem with leaving it out, but it seems that it's good practice to implement this anyways.
	if (bSavedWantsToSprint != ((FSavedMove_Humanoid*)&NewMove)->bSavedWantsToSprint)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

void FSavedMove_Humanoid::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UHumanoidMovementComponent* CharMov = Cast<UHumanoidMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{
		//This is literally just the exact opposite of UpdateFromCompressed flags. We're taking the input
		//from the player and storing it in the saved move.
		bSavedWantsToSprint = CharMov->bWantsToSprint;
	}
}

void FSavedMove_Humanoid::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UHumanoidMovementComponent* CharMov = Cast<UHumanoidMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{}
}

//============================================================================================
//FNetworkPredictionData_Client_Humanoid
//============================================================================================

FNetworkPredictionData_Client_Humanoid::FNetworkPredictionData_Client_Humanoid(const UCharacterMovementComponent& ClientMovement)
	: Super(ClientMovement)
{}

class FNetworkPredictionData_Client* UHumanoidMovementComponent::GetPredictionData_Client() const
{
	//check(PawnOwner != NULL);
	//check(PawnOwner->Role < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UHumanoidMovementComponent* MutableThis = const_cast<UHumanoidMovementComponent*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_Humanoid(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

FSavedMovePtr FNetworkPredictionData_Client_Humanoid::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_Humanoid());
}

