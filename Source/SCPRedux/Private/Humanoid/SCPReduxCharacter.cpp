// SCP

#include "SCPReduxCharacter.h"

#include "TimerManager.h"


#include "Engine/World.h"
#include "Components/InputComponent.h"

#include "HumanoidMovementComponent.h"

#include "InteractableActor.h"


DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ASCPReduxCharacter

ASCPReduxCharacter::ASCPReduxCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UHumanoidMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Setup turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Setup Blinking
	MaxBlinkTime = 13.00f;
	bIsBlinking = false;

	// Setup Interactables
	MaxUseDistance = 400;
	bHasNewFocus = true;
}

void ASCPReduxCharacter::SetupPlayerInputComponent(class UInputComponent* InInputComponent)
{
	check(InInputComponent);
	// Setup Movement
	InInputComponent->BindAxis("MoveForward", this, &ASCPReduxCharacter::MoveForward);
	InInputComponent->BindAxis("MoveRight", this, &ASCPReduxCharacter::MoveRight);

	InInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	InInputComponent->BindAxis("TurnRate", this, &ASCPReduxCharacter::TurnAtRate);
	InInputComponent->BindAxis("LookUpRate", this, &ASCPReduxCharacter::LookUpAtRate);

	// InInputComponent->BindAction("Sprint", IE_Pressed, this, &ASCPReduxCharacter::StartSprinting);
	// InInputComponent->BindAction("Sprint", IE_Released, this, &ASCPReduxCharacter::StopSprinting);

	//InInputComponent->BindAction("Interact", IE_Pressed, this, &ASCPReduxCharacter::Interact);

	
}

void ASCPReduxCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	// Start Blink Timer
	BlinkTimer();
}

/*
Update actor currently being looked at by player.
*/
void ASCPReduxCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Controller && Controller->IsLocalController())
	{
		//AInteractableActor* InteractableActor = GetInteractableInView();
		AInteractableActor* InteractableActor = this->GetClosestInteractableNearSelf();
		
		// End Focus
		if (FocusedInteractableActor != InteractableActor)
		{
			if (FocusedInteractableActor)
			{
				FocusedInteractableActor->OnEndFocus();
			}

			bHasNewFocus = true;
		}

		// Assign new Focus
		FocusedInteractableActor = InteractableActor;

		// Start Focus.
		if (InteractableActor)
		{
			if (bHasNewFocus)
			{
				InteractableActor->OnBeginFocus();
				bHasNewFocus = false;
			}
		}
	}
}


void ASCPReduxCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	// Make sure the blink timers are cleared
	GetWorld()->GetTimerManager().ClearTimer(BlinkTimerHandle);

	// Alternatively clear ALL timers.
	/*GetWorld()->GetTimerManager().ClearAllTimersForObject(this);*/
}

void ASCPReduxCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is forward
		const FRotator Rotation = GetControlRotation();
		FRotator YawRotation(0, Rotation.Yaw, 0);

		// Get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ASCPReduxCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is right
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// Get right vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ASCPReduxCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASCPReduxCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASCPReduxCharacter::StopSprinting()
{
	UHumanoidMovementComponent* MoveComp = Cast<UHumanoidMovementComponent>(GetCharacterMovement());
	if (MoveComp)
	{
		MoveComp->SetSprinting(false);
	}
}

void ASCPReduxCharacter::StartSprinting()
{
	UHumanoidMovementComponent* MoveComp = Cast<UHumanoidMovementComponent>(GetCharacterMovement());
	if (MoveComp)
	{
		if (this->CanISprint())
		{ 
			MoveComp->SetSprinting(true);
		}
		else
		{
			StopSprinting();
		}
	}
}

void ASCPReduxCharacter::BlinkTimer()
{
	this->StimulateBlinking();
	RestartBlinkTimer();
}

void ASCPReduxCharacter::RestartBlinkTimer()
{
	if ( GetWorld()->GetTimerManager().IsTimerActive(BlinkTimerHandle) )
	{
		StopBlinkTimer();
		StartBlinkTimer();
	}
	else
	{
		StartBlinkTimer();
	}
}

void ASCPReduxCharacter::StartBlinkTimer()
{
	if ( !GetWorld()->GetTimerManager().IsTimerActive(BlinkTimerHandle) )
	{
		UE_LOG(LogFPChar, Warning, TEXT("Eyes are opening."));
		bIsBlinking = false;

		//this->StopBlinking();
		GetWorldTimerManager().SetTimer(BlinkTimerHandle, this, &ASCPReduxCharacter::BlinkTimer, MaxBlinkTime, false);
	}
}

void ASCPReduxCharacter::StopBlinkTimer()
{
	if ( GetWorld()->GetTimerManager().IsTimerActive(BlinkTimerHandle) )
	{
		UE_LOG(LogFPChar, Warning, TEXT("Eyes are closing."));
		bIsBlinking = true;

		//this->StartBlinking();
		GetWorld()->GetTimerManager().ClearTimer(BlinkTimerHandle);
	}
}

/*
Performs raytrace to find closest looked-at UsableActor.
*/
AInteractableActor* ASCPReduxCharacter::GetInteractableInView()
{
	FVector camLoc;
	FRotator camRot;

	if (Controller == nullptr)
	{
		return nullptr;
	}

	Controller->GetPlayerViewPoint(camLoc, camRot);
	const FVector StartTrace = camLoc;
	const FVector direction = camRot.Vector();
	const FVector EndTrace = StartTrace + (direction * MaxUseDistance);

	FCollisionQueryParams TraceParams(FName(TEXT("")), true, this);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.bTraceComplex = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility, TraceParams);

	return Cast<AInteractableActor>(Hit.GetActor());

}

/*
Runs on Server. Perform "OnInteract" on currently viewed UsableActor if implemented.
*/
void ASCPReduxCharacter::Interact_Implementation()
{
	//AInteractableActor* InteractableActor = GetInteractableInView();
	AInteractableActor* InteractableActor = this->GetClosestInteractableNearSelf();
	if (InteractableActor)
	{
		InteractableActor->OnInteract(this);
	}
}

bool ASCPReduxCharacter::Interact_Validate()
{
	// No special server-side validation performed.
	return true;
}
