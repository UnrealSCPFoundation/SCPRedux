// SCP 

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"

#include "InteractableActor.h"

#include "SCPReduxCharacter.generated.h"

class UInputComponent;

UCLASS(Blueprintable)
class ASCPReduxCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASCPReduxCharacter();


	UFUNCTION(BlueprintCallable, Category = "Humanoid")
		void StopSprinting();
	UFUNCTION(BlueprintCallable, Category = "Humanoid")
		void StartSprinting();

	UPROPERTY(BlueprintReadWrite, Category = "Human|Eyes")
		float MaxBlinkTime;
	UPROPERTY(BlueprintReadWrite, Category = "Human|Eyes")
		FTimerHandle BlinkTimerHandle;
	UPROPERTY(BlueprintReadWrite, Category = "Human|Eyes")
		bool bIsBlinking;
	UFUNCTION(BlueprintCallable, Category = "Human|Eyes")
		void BlinkTimer();

	UFUNCTION(BlueprintCallable, Category = "Human|Eyes")
		void RestartBlinkTimer();
	UFUNCTION(BlueprintCallable, Category = "Human|Eyes")
		void StartBlinkTimer();
	UFUNCTION(BlueprintCallable, Category = "Human|Eyes")
		void StopBlinkTimer();

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Close Eyes"), Category = "Human|Eyes")
		void StartBlinking();
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Open Eyes"), Category = "Human|Eyes")
		void StopBlinking();
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Stimulate Blinking"), Category = "Human|Eyes")
		void StimulateBlinking();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Human|Sprinting")
		void CalculateFootStep();

	/** Use the actor currently in view (if derived from InteractableActor) */
	UFUNCTION(BlueprintCallable, WithValidation, Server, Reliable, Category = "Pawn")
		virtual void Interact();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	/** Get actor derived from InteractableActor currently looked at by the player */
	UFUNCTION(BlueprintCallable, Category = "Human|Interactable")
		class AInteractableActor* GetInteractableInView();

	UFUNCTION(BlueprintImplementableEvent, Category = "Human|Interactable")
		class AInteractableActor* GetClosestInteractableNearSelf();

	UFUNCTION(BlueprintImplementableEvent, Category = "Human|Sprinting")
		bool CanISprint();

	/* Max distance to use/focus on actors. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float MaxUseDistance;
protected:
	ASCPReduxCharacter(const FObjectInitializer & ObjectInitializer);
	virtual void BeginPlay();
	virtual void Tick(float DeltaSeconds);
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);
	virtual void SetupPlayerInputComponent(class UInputComponent* InInputComponent);


	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	// Interactables
	/* True only in first frame when focused on new usable actor. */
	bool bHasNewFocus;

	/* Actor derived from InteractableActor currently in center-view. */
	AInteractableActor* FocusedInteractableActor;
};

