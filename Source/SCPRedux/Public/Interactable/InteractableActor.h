// SCP

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "InteractableActor.generated.h"

UCLASS()
class SCPREDUX_API AInteractableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
		bool OnInteract(ACharacter* Character);

	UFUNCTION(BlueprintImplementableEvent)
		bool OnBeginFocus();

	UFUNCTION(BlueprintImplementableEvent)
		bool OnEndFocus();
	
};
