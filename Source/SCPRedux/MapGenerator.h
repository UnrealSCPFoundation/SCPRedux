#pragma once

#include "Enumerations.h"
#include "Structures.h"
#include "Engine/DataTable.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MapGenerator.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SCPREDUX_API UMapGenerator : public UObject
{
	GENERATED_BODY()
	
public:
	UMapGenerator();

	UFUNCTION(BlueprintCallable)
	TArray<FRoomSpawn> GenerateMap();

private:
	void Init();
	void ParseDataTable();
	TArray<int32> RandomIndices(int32 Min, int32 Max, int32 Number);
	void PlaceSpecial(TArray<int32> HtEPositions, TArray<int32> LtHPositions);
	void PlaceLongHall(int32 MinX, int32 MaxX, int32 YPosition);
	void PlaceShortHall(int32 MinY, int32 MaxY, int32 XPosition);
	TArray<bool> GetSurroundings(int32 X, int32 Y);
	ERoomShapes GetShape(int32 X, int32 Y);
	ERoomZones GetZone(int32 X, int32 Y);
	FName ChooseRoom(ERoomZones Zone, ERoomShapes Shape);
	FName GetNextRoom(TArray<FName>& Uniques, TMap<FName, float>& Generics);
	void DetermineRooms();
	void DetermineRotations();
	bool ReportUnplacedRooms();
	TArray<FRoomSpawn> PackageMap();
	void GenerateZone(int32 MinX, int32 MaxX, int32 MinY, int32 MaxY, int32 NumHalls, TArray<int32> TopConstraint, TArray<int32> BottomConstraint, bool MoreCorners);
	void RemoveHCZTile(TArray<int32> TopConstraint, TArray<int32> BottomConstraint);
	void GenerateEndcaps(int32 Start, int32 End, int32 DesiredNumber);
	void MutateChances();
	void Reset();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn))
	FRandomStream Rand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn))
	UDataTable* RoomsTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn))
	int32 MapWidth = 15;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn))
	int32 MapHeight = 17;

private:
	int32 EZStart;
	int32 EZEnd;
	int32 HCZStart;
	int32 HCZEnd;
	int32 LCZStart;
	int32 LCZEnd;

	int32 EZLeft;
	int32 EZRight;
	int32 HCZLeft;
	int32 HCZRight;
	int32 LCZLeft;
	int32 LCZRight;

	TArray<TArray<FName>> Map;
	TArray<TArray<FRotator>> RotMap;

	TMap<FName, TSubclassOf<ARoomBase>> ClassRelation;

	TMap<FName, float> LCZGenericEndcaps;
	TMap<FName, float> LCZGenericHallways;
	TMap<FName, float> LCZGenericCorners;
	TMap<FName, float> LCZGenericThreeways;
	TMap<FName, float> LCZGenericFourways;

	TArray<FName> LCZUniqueEndcaps;
	TArray<FName> LCZUniqueHallways;
	TArray<FName> LCZUniqueCorners;
	TArray<FName> LCZUniqueThreeways;
	TArray<FName> LCZUniqueFourways;

	TMap<FName, float> HCZGenericEndcaps;
	TMap<FName, float> HCZGenericHallways;
	TMap<FName, float> HCZGenericCorners;
	TMap<FName, float> HCZGenericThreeways;
	TMap<FName, float> HCZGenericFourways;

	TArray<FName> HCZUniqueEndcaps;
	TArray<FName> HCZUniqueHallways;
	TArray<FName> HCZUniqueCorners;
	TArray<FName> HCZUniqueThreeways;
	TArray<FName> HCZUniqueFourways;

	TMap<FName, float> EZGenericEndcaps;
	TMap<FName, float> EZGenericHallways;
	TMap<FName, float> EZGenericCorners;
	TMap<FName, float> EZGenericThreeways;
	TMap<FName, float> EZGenericFourways;

	TArray<FName> EZUniqueEndcaps;
	TArray<FName> EZUniqueHallways;
	TArray<FName> EZUniqueCorners;
	TArray<FName> EZUniqueThreeways;
	TArray<FName> EZUniqueFourways;
};
